"""
Python Runner module contains a generic class that provides a set of methods
that can be used on the base executor python image.
"""

import conducto


class PythonRunner:
    """
    PythonRunner class.
    This class creates a python image as class instance variable based on the version string
    passed in.
    Several methods are available to be executed individually (or together) on that image.
    """
    REPO_FOLDER_NAME = "ci_repo_dir"

    def __init__(self, repo_url: str, repo_branch: str = "master",
                 python_version: str = "3.8") -> None:
        """
        :param repo_url: str, git repository url to be cloned.
        :param repo_branch: str, branch to be checked out on the cloned git repository.
        :param python_version: str, python version to be used for the docker image.
        """
        super().__init__()
        self.repo_url = repo_url
        self.repo_branch = repo_branch
        self.image = conducto.Image(f"python:{python_version}")

    def initial_setup(self) -> conducto.Exec:
        """
        Method that takes care of initial setup steps.
        """
        return conducto.Exec("apt-get install git make", image=self.image)

    def pip_pipenv_setup(self) -> conducto.Exec:
        """
        Method that takes care of upgrading pip and installing pipenv setup steps.
        """
        return conducto.Exec("python3 -m pip install -U pip pipenv", image=self.image)

    def show_info(self) -> conducto.Exec:
        """
        Method that prints python version info before proceeding.
        """
        return conducto.Exec("python3 --version && python3 -m pip --version && pipenv --version",
                             image=self.image)

    def get_repo(self) -> conducto.Exec:
        """
        Method that clones the repo to the specific branch.
        """
        return conducto.Exec(f"git clone -v -b {self.repo_branch} {self.repo_url}"
                             f" {self.REPO_FOLDER_NAME}",
                             image=self.image)

    def install_dependencies(self) -> conducto.Exec:
        """
        Method that enters the repo directory and installs the dependencies.
        """
        return conducto.Exec(f"cd {self.REPO_FOLDER_NAME} && pipenv install --dev --skip-lock",
                             image=self.image)

    def execute_all(self) -> conducto.Serial:
        """
        Method that performs all tasks serially.
        """
        runner_root = conducto.Serial()
        runner_root['initial_setup'] = self.initial_setup()
        runner_root['pip_pipenv_setup'] = self.pip_pipenv_setup()
        runner_root['show_info'] = self.show_info()
        runner_root['get_repo'] = self.get_repo()
        runner_root['install_dependencies'] = self.install_dependencies()
        return runner_root
