# conducto-ci

Ensure that [conducto] is installed locally or run `pipenv install && pipenv shell`

## Usage

Basic Test: Test function for simply setting up a CI with python project with pipenv

```bash
python3 main.py basic_test --local
```

---

Serial Test: Test function for running CI for PersonalSetup project with serial make commands.

```bash
python3 main.py serial_test --local
```

---

Parallel Test: Test function for running multiple instances in parallel on different versions of python

```bash
python3 main.py parallel_test --local
```
or

```bash
python3 main.py --local
```

[conducto]: <https://pypi.org/project/conducto/>
