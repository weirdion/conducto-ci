import conducto
from personal_setup import PersonalSetup
from python_runner import PythonRunner


def basic_test() -> conducto.Node:
    """
    Test function for simply setting up a CI with python project with pipenv
    """
    runner = PythonRunner(repo_url="https://gitlab.com/asadana/personal_setup.git",
                          repo_branch="develop",
                          python_version="3.8")
    return runner.execute_all()


def serial_test() -> conducto.Node:
    """
    Test function for running PersonalSetup which runs serial commands.
    """
    return PersonalSetup().execute_all()


def parallel_test() -> conducto.Node:
    """
    Test function for running multiple instances of PersonalSetup in parallel on different
    versions of python.
    NOTE: This has been modified to only run on 3.8 versions in parallel since
    conducto/worker-3.9 is not available for testing purposes.
    """
    parallel_runner = conducto.Parallel()
    for version in ["3.8", "latest", "3.8.2"]:
        parallel_runner[f"python_{version}"] = PersonalSetup(python_version=version).execute_all()

    return parallel_runner


if __name__ == "__main__":
    print(__doc__)
    conducto.main(cpu=2, default=parallel_test)
