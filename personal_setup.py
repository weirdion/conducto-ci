"""
Personal Setup module is specific to handling personal_setup.git Makefile execution.
"""
import conducto

from python_runner import PythonRunner

REPO_URL = "https://gitlab.com/asadana/personal_setup.git"
BRANCH_NAME = "develop"


class PersonalSetup(PythonRunner):
    """
    PersonalSetup extends PythonRunner.
    This class adds specific methods that use Makefile pre-defined functions.
    """

    def __init__(self, python_version: str = "3.8") -> None:
        super().__init__(REPO_URL, BRANCH_NAME, python_version)

    def lint_check(self) -> conducto.Exec:
        """
        Method that runs pylint on the source files.
        """
        return conducto.Exec(f"cd {self.REPO_FOLDER_NAME} && make lint-ci",
                             image=self.image)

    def run_tests(self) -> conducto.Exec:
        """
        Method that runs tests with pytest.
        """
        return conducto.Exec(f"cd {self.REPO_FOLDER_NAME} && make test", image=self.image)

    def execute_all(self) -> conducto.Serial:
        runner_root = super().execute_all()
        runner_root["lint_check"] = self.lint_check()
        runner_root["run_tests"] = self.run_tests()
        return runner_root
